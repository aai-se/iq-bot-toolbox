@echo off
@setlocal enableextensions

echo ... Stopping Bot Insight services ...
net stop "Automation Anywhere Bot Insight Elastic Search"
net stop "Automation Anywhere Bot Insight EDC"
net stop "Automation Anywhere Bot Insight Query Engine"
net stop "Automation Anywhere Bot Insight Scheduler"
net stop "Automation Anywhere Bot Insight Service"
net stop "Automation Anywhere Bot Insight Service Discovery"
net stop "Automation Anywhere Bot Insight Visualization"

echo ... Setting Bot Insight services to Manual Start only ...
sc config "Automation Anywhere Bot Insight Elastic Search" start=demand
sc config "Automation Anywhere Bot Insight EDC" start=demand
sc config "Automation Anywhere Bot Insight Query Engine" start=demand
sc config "Automation Anywhere Bot Insight Scheduler" start=demand
sc config "Automation Anywhere Bot Insight Service" start=demand
sc config "Automation Anywhere Bot Insight Service Discovery" start=demand
sc config "Automation Anywhere Bot Insight Visualization" start=demand

echo All Done!


