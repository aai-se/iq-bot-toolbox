@echo off
@setlocal enableextensions

echo ... Stopping Bot Insight services ...
net stop "Automation Anywhere Bot Insight Elastic Search"
net stop "Automation Anywhere Bot Insight EDC"
net stop "Automation Anywhere Bot Insight Query Engine"
net stop "Automation Anywhere Bot Insight Scheduler"
net stop "Automation Anywhere Bot Insight Service"
net stop "Automation Anywhere Bot Insight Service Discovery"
net stop "Automation Anywhere Bot Insight Visualization"

echo All Done!


