@echo off
@setlocal enableextensions


echo ... Stopping Control Room services ...
net stop "Automation Anywhere Control Room Caching"
net stop "Automation Anywhere Control Room Messaging"
net stop "Automation Anywhere Control Room Service"

echo ... Waiting a little ...
ping -n 60 127.0.0.1 >nul

echo ... Starting Control Room services ...
net start "Automation Anywhere Control Room Caching"
net start "Automation Anywhere Control Room Messaging"
net start "Automation Anywhere Control Room Service"

echo All Done!


