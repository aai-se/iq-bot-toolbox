@echo off
@setlocal enableextensions
REM @cd /d "%~dp0"

SET ELASTICSEARCHPATH=C:\ProgramData\AutomationAnywhere\
SET ELASTICSEARCHFOLDERNAME=elasticsearch
set datetimef=%date:~-4%%date:~0,2%%time:~0,2%%time:~3,2%%time:~6,2%

REM taskkill /F /IM "Automation.Cognitive.Documentclassifier.exe"

REM nssm.exe stop "Automation Anywhere Bot Insight Elastic Search"
REM nssm.exe stop "Automation Anywhere Elastic Search Service"

echo ... Stopping Elastic Search services ...
net stop "Automation Anywhere Bot Insight Elastic Search"
net stop "Automation Anywhere Elastic Search Service"

echo Moving the elasticsearch folder to: %ELASTICSEARCHPATH%%ELASTICSEARCHFOLDERNAME% %ELASTICSEARCHPATH%%ELASTICSEARCHFOLDERNAME%%datetimef%
move %ELASTICSEARCHPATH%%ELASTICSEARCHFOLDERNAME% %ELASTICSEARCHPATH%%ELASTICSEARCHFOLDERNAME%%datetimef%

echo ... Starting Elastic Search services ...
net start "Automation Anywhere Bot Insight Elastic Search"
net start "Automation Anywhere Elastic Search Service"

echo All Done!


