# IQ-Bot-Toolbox
collection of scripts for IQ Bot & Control Room

- Fix_audit_logs_error.bat (Fix known audit log error message)
- Shutdown_BotInsight_Services.bat (shutdown BotInsight Services)
- Shutdown_and_Disable_BotInsight_Services.bat (shutdown and disable BotInsight Services)