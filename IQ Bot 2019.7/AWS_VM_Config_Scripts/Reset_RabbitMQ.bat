@echo off

cd "C:\Program Files\RabbitMQ Server\rabbitmq_server-3.6.6\sbin"

rabbitmqctl stop_apprabbitmqctl reset
rabbitmqctl start_app
Rabbitmqctl add_user messagequeue passmessage
Rabbitmqctl add_vhost test
rabbitmqctl set_permissions -p test messagequeue ".*" ".*" ".*"
rabbitmqctl set_user_tags messagequeue administrator