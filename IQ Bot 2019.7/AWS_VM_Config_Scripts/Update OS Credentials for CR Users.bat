@echo off
cd "C:\Program Files\Automation Anywhere Extensions\Command Line Interface"

set /p pwd="Enter New Administrator Password for this VM: "

echo Authenticating to CR: python AUTH_utils.py -o login -l admin -p "Un1ver$e123" -u "http://localhost" -s MySession
python AUTH_utils.py -o login -l admin -p "Un1ver$e123" -u "http://localhost" -s MySession
echo Updating Passwords: python USERS_utils.py -o setlogin -u "iqbot1,iqbot2,iqbot3,iqbot4,iqbot5,iqbot6,iqbot7,iqbot8,iqbot9,iqbot10" -n "Administrator" -w "%pwd%" -s MySession

python USERS_utils.py -o setlogin -u "iqbot1,iqbot2,iqbot3,iqbot4,iqbot5,iqbot6,iqbot7,iqbot8,iqbot9,iqbot10" -n "Administrator" -w "%pwd%" -s MySession

pause