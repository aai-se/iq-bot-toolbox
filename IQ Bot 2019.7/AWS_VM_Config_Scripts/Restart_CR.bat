@echo off
@setlocal enableextensions


echo ... Stopping Control Room Services ...

net stop "Automation Anywhere Control Room Reverse Proxy"
net stop "Automation Anywhere Control Room Caching"
net stop "Automation Anywhere Control Room Messaging"
net stop "Automation Anywhere Control Room Service"
net stop "Automation Anywhere Elastic Search Service"


echo ... Waiting a little ...
ping -n 10 127.0.0.1 >nul

echo ... Starting Control Room Services ...
net start "Automation Anywhere Control Room Reverse Proxy"
net start "Automation Anywhere Control Room Caching"
net start "Automation Anywhere Control Room Messaging"
net start "Automation Anywhere Control Room Service"
net start "Automation Anywhere Elastic Search Service"

echo All


