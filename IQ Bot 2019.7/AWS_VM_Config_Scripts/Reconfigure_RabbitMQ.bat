@echo off
@setlocal enableextensions

cd C:\Program Files\RabbitMQ Server\rabbitmq_server-3.6.6\sbin
echo ... Reconfiguring RabbitMQ Service ...
SET HOMEDRIVE=C:
echo  Stopping RabbitMQ Service
call rabbitmq-service stop
echo  Removing RabbitMQ Service
call rabbitmq-service remove
echo  Installing RabbitMQ Service
call rabbitmq-service install
echo  Starting RabbitMQ Service
call rabbitmq-service start
exit