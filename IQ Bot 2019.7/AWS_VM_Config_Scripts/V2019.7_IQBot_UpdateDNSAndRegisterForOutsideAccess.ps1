###
#
# Purpose: Reconfigure IQ Bot and Control Room A2019.7 after cloning a VM / changing the hostname
# Author: Bren Sapience
#
# Updates:
#     - Dec 2nd 2019: Revamp of the script for A2019.7
#
#
###


###
#
# Setting Variables
#
###

# Control Room Port
$CRPort = "80"

# Control Room Authentication URI
$CRAuthenticationURI = 'http://localhost:'+$CRPort+'/v1/authentication'

# Control Room IQ Bot Registration URI
$CRRegistrationURI = 'http://localhost:'+$CRPort+'/v1/app/IQBOT'

# Install Paths
$IQBotInstallBasePath = "C:\Program Files (x86)\Automation Anywhere IQ Bot 6.5\"
$CRInstallBasePath = "C:\Program Files\Automation Anywhere\Enterprise\"

# SQL Database Coordinates
$DBInstance = "localhost\SQLEXPRESS"
$DBLogin = "sa"
$DBPwd = "@dmin@123"

# Control Room Admin Credentials (for IQ Bot Registration)
$iqbotcreds = @{
    username='admin'
    password='Un1ver$e123'
}

# SQL Instance Connection String for IQ Bot Configurations DB
$DBConnString = 'Data Source={0};Initial Catalog=Configurations;User ID={1};Password={2}' -f $DBInstance,$DBLogin,$DBPwd

# Current Hostname
$hostname = hostname.exe

# Config Files for IQ Bot and Control Room
$IQBotSettingsFile = $IQBotInstallBasePath + "Configurations\Settings.txt"
$IQBotConfigurationFile = $IQBotInstallBasePath + "Configurations\configuration.txt"
$CRBootDBFile = $CRInstallBasePath + "config\boot.db.properties"

# Names to use to backup config files
$IQBotSettingsFileBCKUP = $IQBotInstallBasePath + "Configurations\Settings.txt.backup"
$IQBotConfigurationFileBCKUP = $IQBotInstallBasePath + "Configurations\configuration.txt.backup"
$CRBootDBFileBCKUP = $CRInstallBasePath + "config\boot.db.properties.backup"

# Hostname of the original VM that needs to be replaced
$DefaultHostname = 'EC2AMAZ-40PJLMI'
$FolderToDelete = "C:\Users\Administrator.EC2AMAZ-40PJLMI"


#######################

####### FUNCTIONS

#######################

#Test connectivity to Control Room (used when restarting the Control Room to ensure proper status before continuing
class test_CR_conn {
    [int]checkCRConn($CRAuthenticationURI) {
        
      try {
        $response = Invoke-RestMethod $CRAuthenticationURI -Method Get -ContentType 'application/json'
      } catch {
            # Dig into the exception to get the Response details.
            # Note that value__ is not a typo.
            return $_.Exception.Response.StatusCode.value__ 
           #rite-Host "StatusDescription:" $_.Exception.Response.StatusDescription
           # return False
      }
      return $response.StatusCode
    }
}


#######################

####### MAIN()

#######################


Write-Host " "
Write-Host "!! IMPORTANT NOTE: This Script will Require a REBOOT of the server at the end.. !!"
Write-Host " "


$installlocation = $IQBotInstallBasePath

# Getting the new IP, FQDN to use
$dns = Read-Host "Enter the Public FQDN or Public IP address of this host, (e.g. 10.1.2.3 or {EXAMPLE}.compute-1.amazonaws.com):`n"
$dns = $dns.Trim()
Write-Host "`n"

# Making sure it isnt empty
if (!$dns) {
    Write-Host "Error: Public IP or FQDN cannot be empty."
    exit
}

# Create url for CR and IQ Bot
$CRurl = "http://" + $dns + ":" +$CRPort
$IQBOTurl = "http://" + $dns + ":3000"


###
# STEP 1 = Replace old hostname in Config Files for IQ Bot and Control Room
###

Write-Host " "
Write-Host "Step 1 = Replace Hostname in IQ Bot Settings Configuration files and CR configuration file (new Hostname: $hostname)"
Write-Host " "

# Backup Config Files just in case	
copy-item -path $IQBotSettingsFile -destination $IQBotSettingsFileBCKUP
copy-item -path $IQBotConfigurationFile -destination $IQBotConfigurationFileBCKUP
copy-item -path $CRBootDBFile -destination $CRBootDBFileBCKUP

# Replace Hostname in config files
try{
    Start-Sleep  -s 1
    ((Get-Content -path $IQBotSettingsFile -Raw -ErrorAction Stop) -replace $DefaultHostname,$hostname) | Set-Content -Path $IQBotSettingsFile -ErrorAction Stop
    Write-Host "    -- OK: Replaced hostname in file $IQBotSettingsFile"
    Start-Sleep  -s 1
    ((Get-Content -path $IQBotConfigurationFile -Raw -ErrorAction Stop) -replace $DefaultHostname,$hostname) | Set-Content -Path $IQBotConfigurationFile -ErrorAction Stop
    Write-Host "    -- OK: Replaced hostname in file $IQBotConfigurationFile"
    Start-Sleep  -s 1
    ((Get-Content -path $CRBootDBFile -Raw -ErrorAction Stop) -replace $DefaultHostname,$hostname) | Set-Content -Path $CRBootDBFile -ErrorAction Stop
    Write-Host "    -- OK: Replaced hostname in file $CRBootDBFile"
    Start-Sleep  -s 1
}catch{
    # Occasionally, Windows fails to access the files.. in which case a simple rerun solves the issue.
    Write-Host "Error reconfiguring files.. Please Rerun this script."
    copy-item -path $IQBotSettingsFileBCKUP -destination $IQBotSettingsFile
    copy-item -path $IQBotConfigurationFileBCKUP -destination $IQBotConfigurationFile
    copy-item -path $CRBootDBFileBCKUP -destination $CRBootDBFile
}

###
# STEP 2 = Replace Hostname Entries in IQ Bot and CR Databases
###

Write-Host " "
Write-Host "Step 2 = Replace Hostname Entries in Databases and Unregistering IQ Bot."
Write-Host " "

#Create SQL Connection
$con = New-Object System.Data.SqlClient.SqlConnection $DBConnString
$con.Open()
$sqlcmd = new-object "System.data.sqlclient.sqlcommand"
$sqlcmd.connection = $con
$sqlcmd.CommandTimeout = 600000


$sqlcmd.CommandText = "UPDATE [Configurations].[dbo].[CognitivePlatformNodes] SET [LoadBalancerURL] = 'http://localhost:3000' WHERE [NodeName] = '$DefaultHostname';"
$line = $sqlcmd.ExecuteNonQuery()
Write-Host "    -- OK: replaced LoadBalancerURL in IQ Bot DB CognitivePlatformNodes"

$sqlcmd.CommandText = "UPDATE [Configurations].[dbo].[CognitivePlatformNodes] SET [ClientPlatformUrl] = 'http://localhost:3000' WHERE [NodeName] = '$DefaultHostname';"
$line = $sqlcmd.ExecuteNonQuery()
Write-Host "    -- OK: replaced ClientPlatformUrl in IQ Bot DB CognitivePlatformNodes"

# Deregister IQ Bot from Control Room:
$sqlcmd.CommandText = "delete FROM [AAE-Database].[dbo].[SERVICE_USER] where [type]='IQBOT' ;"
$line = $sqlcmd.ExecuteNonQuery()
Write-Host "    -- OK: removed records in SERVICE_USER in Control Room DB (Type=IQBOT)"

$sqlcmd.CommandText = "delete FROM [AAE-Database].[dbo].[SERVICE_USER] where [type]='DEVICE' ;"
$line = $sqlcmd.ExecuteNonQuery()
Write-Host "    -- OK: removed records in SERVICE_USER in Control Room DB (Type=DEVICE)"

$sqlcmd.CommandText = "delete FROM [AAE-Database].[dbo].[USERS] where user_type='APP';"
$line = $sqlcmd.ExecuteNonQuery()
Write-Host "    -- OK: removed records in USERS in Control Room DB (user_type=APP)"

$sqlcmd.CommandText = "delete FROM [AAE-Database].[dbo].[ACTIVEMQ_ACKS];"
$line = $sqlcmd.ExecuteNonQuery()
Write-Host "    -- OK: removed records in ACTIVEMQ_ACKS in Control Room DB"

$sqlcmd.CommandText = "delete FROM [AAE-Database].[dbo].[ACTIVEMQ_MSGS];"
$line = $sqlcmd.ExecuteNonQuery()
Write-Host "    -- OK: removed records in ACTIVEMQ_MSGS in Control Room DB"

$sqlcmd.CommandText = "DELETE FROM [Configurations].[dbo].[Configurations] where [key]='controlRoomVersion';"
$line = $sqlcmd.ExecuteNonQuery()
Write-Host "    -- OK: removed records in Configurations in IQ Bot DB (controlRoomVersion)"

$sqlcmd.CommandText = "DELETE FROM [Configurations].[dbo].[Configurations] where [key]='appRegistered';"
$line = $sqlcmd.ExecuteNonQuery()
Write-Host "    -- OK: removed records in Configurations in IQ Bot DB (appRegistered)"

$sqlcmd.CommandText = "DELETE FROM [Configurations].[dbo].[Configurations] where [key]='controlRoomUrl';"
$line = $sqlcmd.ExecuteNonQuery()
Write-Host "    -- OK: removed records in Configurations in IQ Bot DB (controlRoomUrl)"

$sqlcmd.CommandText = "DELETE FROM [Configurations].[dbo].[Configurations] where [key]='appId';"
$line = $sqlcmd.ExecuteNonQuery()
Write-Host "    -- OK: removed records in Configurations in IQ Bot DB (appId)"

$con.close()

###
# STEP 3 = Reinstall IQ Bot Services
###

Write-Host " "
Write-Host "Step 3 = Re-Install IQ Bot Services."
Write-Host " "

$stopAndUninstall = $IQBotInstallBasePath+"Configurations\stopanduninstallallservices.bat"
Start-Process $stopAndUninstall -Wait

Write-Host "    -- OK: IQ Bot Services Uninstalled"
Start-Sleep  -s 5

$installAndStart = $IQBotInstallBasePath+"Configurations\installandstartallservices.bat"
Start-Process $installAndStart -Wait

Write-Host "    -- OK: IQ Bot Services Installed and Started"

###
# STEP 4 = Restart Control Room Services (takes a while to come up, so we need to loop until it is back up and running)
###

Write-Host " "
Write-Host "Step 4 = Restart Control Room Services (4 to 10 mins)."
Write-Host " "

$restartCR = "c:\Restart_CR.bat"
Start-Process $restartCR -Wait
Write-Host "    -- OK: Control Room Services Restarted"

$tc = New-Object -TypeName test_CR_conn

$WaitTime = 30 #seconds
$BadGatewayCode = 502

# Loop until the bad gateway error is gone
do{
    $ResCode = $tc.checkCRConn($CRAuthenticationURI)

    if($ResCode -eq $BadGatewayCode){
        #Write-Host "CR Responded with code $BadGatewayCode (Bad Gateway). Waiting $WaitTime Seconds before trying again."
        Write-Host "    -- WAIT: CR Responded with code $BadGatewayCode (Bad Gateway). Waiting $WaitTime Seconds before trying again."
        Start-Sleep -Seconds $WaitTime
    }

}while($ResCode -eq $BadGatewayCode)


Write-Host "    -- OK: Control Room is now Active."

###
# STEP 5 = Generating an Authentication token via the Rest API
###

Write-Host " "
Write-Host "Step 5 = Generate Control Room Authentication Token."
Write-Host " "

# Invoke Rest API
$json = $iqbotcreds | ConvertTo-Json
$URI =$CRAuthenticationURI
$response = Invoke-RestMethod $URI -Method Post -Body $json -ContentType 'application/json'

# Response should return a Token
$token = $response.token

if (!$token) {
    Write-Host "Error, Authentication to CR seems to have failed:"
    Write-Host "$response"
    exit
}else{
    Write-Host "    -- OK: Control Room Authentication Token Generated."
}

# With the Auth Token, we now need to re-register IQ Bot with the public URL
Write-Host " "
Write-Host "Step 6 = Register IQ Bot to Control Room."
Write-Host " "

$headersR = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
$headersR.Add("X-Authorization", $token)

$iqbotregistration = @{
    appUrl="$IQBOTurl"
}

$jsonR = $iqbotregistration | ConvertTo-Json
Write-Host "Registration JSON Body: $jsonR"
Write-Host "Registration URL: $CRRegistrationURI"


try{
     $responseR = Invoke-RestMethod $CRRegistrationURI -Method Post -Body $jsonR -Headers $headersR -ContentType 'application/json'
}
catch{
     $RetCode=$_.Exception.Response.StatusCode.value__ 
     $RetDesc=$_.Exception.Response.StatusDescription
}

if($responseR.id){

  Write-Host "Complete Response: $responseR"
  Write-Host "    -- OK: IQ Bot registered."
  
}else{
    Write-Host "    -- Error: IQ Bot could not be registered."
    exit
}

# After registration, some links in the DB still need to be adjusted
Write-Host " "
Write-Host "Step 7 = Adjusting IQ Bot DB (with remote CR Url in Configurations DB)."
Write-Host " "

#Create SQL Connection
$con = New-Object System.Data.SqlClient.SqlConnection $DBConnString
$con.Open()

$sqlcmd = new-object "System.data.sqlclient.sqlcommand"
$sqlcmd.connection = $con
$sqlcmd.CommandTimeout = 600000

$sqlcmd.CommandText = "UPDATE [Configurations].[dbo].[Configurations] SET [Value] = '$CRurl' WHERE [Key] = 'controlRoomUrl';"
$line = $sqlcmd.ExecuteNonQuery()

$con.close()

###
# STEP 8 = Open Firwall Ports if needed
###

Write-Host " "
Write-Host "Step 8 = Open Firwall Ports."
Write-Host " "
$NumOfRules = (Get-NetFirewallRule -DisplayName 'Automation*' | Format-Table | findstr "Automation Anywhere" | Measure-Object -line).Lines

if($NumOfRules -gt 0){
    Write-Host "    -- OK: Firewall Rule Already Exists: [Matching Rules: $NumOfRules]"
    Write-Host ""
}else{
    New-NetFirewallRule -DisplayName 'Automation Anywhere CR + IQ Bot' -Profile @('Any') -Direction Inbound -Action Allow -Protocol TCP -LocalPort @('80', '443','3000')
    Write-Host "    -- OK: Firewall Rule Added for IQ Bot and Control Room."
}

# Next step is to reconfigure Erlang / RabbitMQ, which requires:
# 1- Shutting down the erlang processes
# 2- Cleaning Up the C:\Users\Administrator.<Old Hostname> folder
# 3- Reset RabbitMQ
# 4- Reconfigure RabbitMQ Services
# 5- Unrelated but necessary at this point: Reinstall IQ Bot Services

Write-Host " "
Write-Host "Step 9 = Shutdown Erlang."
Write-Host " "

Try
{
    stop-process -ProcessName epmd -Force
}
Finally
{
    Write-Host "    -- INFO: epmd Process Shutdown."
}

Try
{
    stop-process -ProcessName erl -Force
}
Finally
{
    Write-Host "    -- INFO: erl Process Shutdown."
}

Try
{
    stop-process -ProcessName erlsrv -Force
}
Finally
{
    Write-Host "    -- INFO: erlsrv Process Shutdown."
}

Write-Host "    -- OK: Erlang Processes Shut Down."


Write-Host " "
Write-Host "Step 10 = Clean up C:\Users\Administrator.<OldHostname> Folder."
Write-Host " "

Try
{
    Remove-Item -LiteralPath "$FolderToDelete" -Force -Recurse
}
Finally
{
    Write-Host "    -- INFO: Folder Already Cleaned. Ignore the error above."
}

Write-Host "    -- OK: Folder Cleaned Up."

Write-Host " "
Write-Host "Step 11 = Re-Install IQ Bot Services."
Write-Host " "

$stopAndUninstall = $IQBotInstallBasePath+"Configurations\stopanduninstallallservices.bat"
Start-Process $stopAndUninstall -Wait
Write-Host "    -- OK: IQ Bot Services Uninstalled"
Start-Sleep  -s 5
$installAndStart = $IQBotInstallBasePath+"Configurations\installandstartallservices.bat"
Start-Process $installAndStart -Wait
Write-Host "    -- OK: IQ Bot Services Installed and Started"

Start-Sleep  -s 5
Write-Host " "
Write-Host "Step 12 = Reconfigure RabbitMQ."
Write-Host " "

$reconfigureRabbitMQ = "C:\Reconfigure_RabbitMQ.bat"
Start-Process $reconfigureRabbitMQ
Write-Host "    -- OK: RabbitMQ Reconfigured"

Start-Sleep  -s 20

Write-Host " "
Write-Host "Step 13 = Starting RabbitMQ."
Write-Host " "

$RabbitMQServiceName = "RabbitMQ"

Start-Service $RabbitMQServiceName
$serviceAfter = Get-Service $RabbitMQServiceName
Write-Host "    -- INFO: RabbitMQ Service Status: "
Get-Service | Where-Object {$_.Name -EQ "$RabbitMQServiceName"}

Write-Host " "
Write-Host "DONE. Please make  sure the output of the script is OK and REBOOT the server for changes to take  effect."
Write-Host " "
Start-Sleep  -s 600
#Restart-Computer