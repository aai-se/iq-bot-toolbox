@echo off
@setlocal enableextensions


echo ... Stopping Control Room Reverse Proxy ...
net stop "Automation Anywhere Control Room Reverse Proxy"
REM net stop "Automation Anywhere Control Room Messaging"
REM net stop "Automation Anywhere Control Room Service"

echo ... Waiting a little ...
ping -n 2 127.0.0.1 >nul

echo ... Starting Control Reverse Proxy ...
net start "Automation Anywhere Control Room Reverse Proxy"
REM net start "Automation Anywhere Control Room Messaging"
REM net start "Automation Anywhere Control Room Service"

echo All Done!


